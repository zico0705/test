import requests
from urllib.error import URLError, HTTPError
from datetime import datetime
import mailsetting

# 상태 코드를 저장할 배열 선언
urlResults = []
headerResults = []
paramResults = []

# 요청시 헤더정보를 agent 토큰으로 지정.
request_headers = {'agent-api-token': 'header 값'}


# http 호출.
def http_get_url(url, results):
    try:
        res = requests.get(url)
        code = res.status_code
        results.append(code)
    except HTTPError as e:
        code = e.getcode()
        print(code)
        results.append(code)


# http 호출_header.
def http_get_header(url, results):
    try:
        res = requests.get(url, headers=request_headers)
        code = res.status_code
        results.append(code)
    except HTTPError as e:
        code = e.getcode()
        print(code)
        results.append(code)


# http post 호출_header, params.
def http_post_param(url, results):
    data = {            #param data 작성.
            "limit": 10,
            "page": 1
            }
    try:
        res = requests.post(url, params=data, headers=request_headers)
        code = res.status_code
        results.append(code)
    except HTTPError as e:
        code = e.getcode()
        print(code)
        results.append(code)


get_url = "https://www."   #get_url 작성.
header_url = "https://www."     #header_url 작성.
param_url = "https://www."      #param_url 작성.


# API Check 함수.
def http_getter():
    http_get_url(get_url, urlResults)
    http_get_header(header_url, headerResults)
    http_post_param(param_url, paramResults)


# HTTP코드 확인 후 분류하는 함수
def CheckHTTPCode(results, errormsglist, normalmsglist):
    for n in range(0, len(results)):

        if results[n] != 200:
            mailsetting.flag = 1
            errormsglist.append(results[n])
            now = datetime.now()
            print(str(now) + ' - ' + '상태 비 정상.')

        elif results[n] == 200:
            normalmsglist.append(results[n])


# API 리턴 값을 HTTP 상태 체크 함수를 이용하여 mail messages List에 저장.
def HTTPChecker():
    CheckHTTPCode(urlResults, mailsetting.uErrorMsgList, mailsetting.uNormalMsgList)
    CheckHTTPCode(headerResults, mailsetting.hErrorMsgList, mailsetting.hNormalMsgList)
    CheckHTTPCode(paramResults, mailsetting.pErrorMsgList, mailsetting.pNormalMsgList)