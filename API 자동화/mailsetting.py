import smtplib
import readjsonfile as rj
import mailsendcondition as msc
import mainnetcheck
from email.mime.text import MIMEText

#업로드 정상여부 구별을 위한 flag 변수생성
flag = 0

#QM메일 내용을 담을 배열 선언
contents = []

#그룹 메일 내용 배열
groupContents = []

#정상 비정상 별 결과를 저장하기 위한 리스트 선언
oneErrorMsgList = []
twoErrorMsgList = []
threeErrorMsgList = []

oneNormalMsgList = []
twoNormalMsgList = []
threeNormalMsgList = []

totalErrorMsgList = []


#메일서버 세팅 함수, 라온시큐어 메일서버 접근
def mailServerSetting():
    smtp = smtplib.SMTP_SSL('192.168.0.161', 465)
    smtp.ehlo()
    smtp.login('raonmailtest@raoncorp.net', 'raonmailtest12#$')
    return smtp


#QM메일 발송 함수
def sendMailtoQM(smtp, contentsList, flag, receiver):
    #메일발송체크용 flag
    normalflag = 0
    errorflag = 0

    #배열에 있는 내용을 변수에 담아 메일 내용으로 전달(메일 내용으로 리스트를 사용할 수 없음)
    text = '\n'.join(contentsList)

    #메일 발송
    msg = MIMEText(str(text))
    msg['From'] = 'OmniOne@CHECK'

    if flag == 1: #비정상
        errorflag = 1
        msg['Subject'] = '[비정상] OOOO 상태 비정상. 확인이 필요합니다.'
        msg['To'] = 'Monitoring@CHECK'
        smtp.sendmail('OOOO@OOOO.com', 'OOOO@OOOO.com', msg.as_string())
        pass

    elif flag == 0: #정상
        normalflag = 1
        msg['Subject'] = '[정상] OOOO 상태 정상.'
        msg['To'] = 'Monitoring@CHECK'
        for mailReciverList in receiver:
            smtp.sendmail('OOOO@OOOO.com', mailReciverList, msg.as_string())
    smtp.quit()


#특정그룹에 메일보내기
def sendMailtoGroup(smtp, contentsList, receiver, mailSubject):
    text = '\n'.join(contentsList)
    msg = MIMEText(str(text))
    msg['Subject'] = mailSubject
    msg['From'] = 'Monitoring@CHECK'
    msg['TO'] = 'Monitoring@CHECK'
    for mailReciverList in receiver:
        smtp.sendmail('OOOO@OOOO.com', mailReciverList, msg.as_string())
    smtp.quit()


#txt에 있는 메일 내용 가져오는 함수
def getTxtMailContents(path, contentsList):
    f = open(path, 'r', encoding='utf-8')
    while True:
        line = f.readline()
        if not line: break
        contentsList.append(line.strip('\n'))
    f.close()


#메일 내용 작성 함수
def writeMailContents(msglist, apiname, url, contentsList):
    if len(msglist) > 0:
        for n in range(0, len(msglist)):
            contentsList.append('---------------------------------------------------------------------------------------------------------------------------')
            contentsList.append(apiname + '  ||  ' + 'HTTP status code : [' + str(msglist[n]) + ']  ||  ' + url)
            contentsList.append('---------------------------------------------------------------------------------------------------------------------------')


#앱별 404 메일 작성
def write404MailEachApp(msgList, url, receiverAppList, contentsList, receiver):
    cnt = 0
    for outerForCount in msgList:
        for innerForCount in receiverAppList:
            if outerForCount == innerForCount:
                contentsList.clear()
                mailSubject = '[' + innerForCount + '] OmniOne Mainnet 상태 비정상. 확인이 필요합니다.'
                getTxtMailContents('C:/omnione_check/config/mailcontents/errorMailcontents/groupUpperPart.txt', contentsList)
                contentsList.append(str(innerForCount) + '  ||  ' + 'HTTP status code : [' + str(msgList[cnt]) + ']  ||  ' + url)
                getTxtMailContents('C:/omnione_check/config/mailcontents/errorMailcontents/groupBelowPart.txt', contentsList)
                sendMailtoGroup(mailServerSetting(), contentsList, receiver, mailSubject)
        cnt += 1


def mailSender():
        #아래 if문 안의 내용은 업로드 상태 비정상인 경우 작성함(QM에만 보내는 메일)
    if flag == 1:
        getTxtMailContents('C:/omnione_check/config/mailcontents/errorMailcontents/upperPart.txt', contents)
        writeMailContents(oneErrorMsgList, 'Chain_Info', mainnetcheck.chain_url, contents)
        writeMailContents(twoErrorMsgList, 'Agent_Version', mainnetcheck.agent_ver_url, contents)
        writeMailContents(threeErrorMsgList, 'Agent_Transaction', mainnetcheck.agent_tran_url, contents)
        getTxtMailContents('C:/omnione_check/config/mailcontents/errorMailcontents/groupBelowPart.txt', contents)
        contents.append('\n')
            
    #메일 발송을 위한 내용 작성 후 배열에 저장, 아침9시 ~ 밤8시 까지만 정상일 때도 메일 발송
    if flag == 1 or msc.hollydayCheck() == 1:
        if flag == 1 or msc.timecheck() == 1:
            getTxtMailContents('C:/폴더경로/upperPart.txt', contents)
            writeMailContents(oneNormalMsgList, "OOO_API", mainnetcheck.chain_url, contents)
            writeMailContents(twoNormalMsgList, "OOO_API", mainnetcheck.agent_ver_url, contents)
            writeMailContents(threeNormalMsgList, "OOO_API", mainnetcheck.agent_tran_url, contents)
            getTxtMailContents('C:/폴더경로/belowPart.txt', contents)


def groupMailSender():
    totalErrorMsgList = oneErrorMsgList + twoErrorMsgList + threeErrorMsgList
    write404MailEachApp(totalErrorMsgList, mainnetcheck.chain_url, rj.QMMailreceiver, groupContents, rj.QMMailreceiver)