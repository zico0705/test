import mainnetcheck
import readjsonfile
import mailsetting as mst
from datetime import datetime


#일정 시간에만 메일 발송(밤 7시 ~ 아침 9시 이전까지는 업로드 상태 정상이면 메일 발송 안함 // currenttime = 1)
def timecheck():
    today = datetime.today()
    hour = today.hour
    if int(hour) >= 19:
        currenttime = 0
    elif int(hour) <= 8:
        currenttime = 0
    else:
        currenttime = 1
    return currenttime


#공휴일 판단용 hollydays 리스트는 readjsonfile.py에서 생성함
#공휴일 판단을 위해 월, 일 가져오기, 공휴일 리스트(공휴일에는 비정상 메일만 발송합니다.)
#공휴일 리스트는 매년 json 파일에서 수동업데이트 해야합니다.
#공휴일/주말 판단로직, 공휴일 또는 주말이면 isItHollyday = 0, / 1일때 정상메일 발송


def hollydayCheck():
    readjsonfile.getInfoFromJson(readjsonfile.hollydays, 'hollydays')
    isItHollyday = 1
    wd = datetime.now()
    md = datetime.now()
    day = md.day
    month = md.month
    monthAndDay = str(month) + '-' + str(day)
    for count in readjsonfile.hollydays:
        if count == monthAndDay or datetime.weekday(wd) > 4:
            isItHollyday = 0
            break
    return isItHollyday


#3600초 카운트용 변수제어
def tiktokCheck(tik):
    tik = tik * -1
    isTiktok = tik
    return isTiktok

#메일발송조건 테스트용 변수 currenttime(아침9시~저녁7시까지만 발송), isItHollyday(근무일에만 발송), weekDay(토, 일요일에는 정상메일 발송하지 않음)
#1일 때 정상메일 발송
#currenttime = 1
#isItHollyday = 1
#isTiktok = 1


def sendConditionCheck(tikCheck):
    currenttime = timecheck()
    tikCheck = tikCheck
    if mst.flag == 1 or (currenttime == 1 and hollydayCheck() == 1 and tikCheck == 1):
        if len(mainnetcheck.ChainResults) != 0:
            mst.sendMailtoQM(mst.mailServerSetting(), mst.contents, mst.flag, readjsonfile.QMMailreceiver)
