import mainnetcheck
import readjsonfile as rj
import mailsetting as mst
from datetime import datetime


#루프 내에서 계속 사용할 변수와 리스트 초기화하는 함수
def listFlush():
    mst.flag = 0

    mst.contents.clear()

    mst.groupContents.clear()

    mst.oneErrorMsgList.clear()

    mst.twoErrorMsgList.clear()

    mst.threeErrorMsgList.clear()

    mst.oneNormalMsgList.clear()

    mst.twoNormalMsgList.clear()

    mst.threeNormalMsgList.clear()

    mst.totalErrorMsgList.clear()

    rj.hollydays.clear()

    rj.QMMailreceiver.clear()

    mainnetcheck.ChainResults.clear()

    mainnetcheck.VersionResults.clear()

    mainnetcheck.TranResults.clear()

    print(str(datetime.now()) + ' - Monitoring 체크 완료.')
