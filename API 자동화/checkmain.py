import endofcycle
import readjsonfile
import mailsetting as mst
import mailsendcondition as msc
import mainnetcheck
import time
from loggersetting import logger

version = '[v1.0.0 | OOOO Monitoring Check]'
print(version)

#1800초마다 점검하고 메일은 3600초에 한번씩 발송 isTiktok = 1일 때 정상메일 발송
tiktok = -1

# OmniOne Mainnet 체크 사이클 시작
while True:
    #json 파일에서 메일 주소록 가져오기
    readjsonfile.getMailInfo()
    
    #getHTTPCode 실행하여 응답코드 스크래핑
    mainnetcheck.chain_getter()

    #받아온 응답코드를 OS별, 정상, 비정상 별로 분류
    mainnetcheck.HTTPChecker()

    #앱 별 응답코드 404 검증 후 메일링
    mst.groupMailSender()

    #아래 if문 안의 내용은 업로드 상태 비정상인 경우 작성함(QM에만 보내는 메일)
    mst.mailSender()

    #일정 시간에만 메일 발송(저녁 7시 ~ 아침 8시 까지는 업로드 상태 정상이면 메일 발송 안함 currenttime = 1)
    msc.sendConditionCheck(msc.tiktokCheck(tiktok))

    #1800초 마다 점검하고 메일발송은 3600초에 한번 씩 하도록 tiktok 변수 값 변경
    tiktok = msc.tiktokCheck(tiktok)

    #루프 내에서 사용할 모든 변수와 리스트 초기화
    endofcycle.listFlush()

    #1800초 대기 후 다시 체크, 메일은 3600초에 한번 씩 보냄.(비정상은 체크 시 마다 보냄)
    time.sleep(1800)