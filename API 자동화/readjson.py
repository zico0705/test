import json

# 휴일 점검용 리스트 생성
hollydays = []

# QA팀 정상메일 주소록
QAMail = []


# 휴일과 메일 주소록을 json 파일에서 받아오기
def getInfoFromJson(listName, jsonKey):
    with open('C:/api_check/config/maillist/maillist.json', 'r', encoding='utf-8') as mailListFile:
        mailListData = json.load(mailListFile)

    listName.extend(mailListData[jsonKey])


# json 파일에서 메일 주소록 가져오기
def getMailInfo():
    getInfoFromJson(QAMail, 'QA팀 주소록')